import "babel-polyfill";
import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./assets/style.css";
import configureStore from "./store/configureStore";
import App from "./components/App.js";

const store = configureStore();  // can add initial state as a param

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("weatherApp")
);