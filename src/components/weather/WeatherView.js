import React from "react";

class WeatherView extends React.Component {
    render() {
        return (
            <div className="weather-view-block col-xs-8 col-xs-offset-1">
                <div className="weather-view-header">
                    <span className="weather-view-city">London{" - "}Rain</span>
                </div>
                <div className="weather-view-body">
                    <span>Temperature - min/max</span>
                    <span>Pressure - xxx</span>
                    <span>Humidity - xxx</span>
                    <span>Wind - speed direction</span>
                    <span>Clouds - percent</span>
                </div>
            </div>
        );
    }
}

export default WeatherView;