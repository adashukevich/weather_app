import React from "react";
import { connect } from "react-redux";
import * as cityActions from "../../actions/cityActions";

class CityForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            city: {
                name: ""
            }
        };

        this.onCityInputChange = this.onCityInputChange.bind(this);
        this.onAddClick = this.onAddClick.bind(this);
    }

    onCityInputChange(event) {
        const city = this.state.city;
        city.name = event.target.value;
        this.setState({ city: city });
    }

    onAddClick(event) {
        event.preventDefault();
        this.props.dispatch(cityActions.addCity(this.state.city));
    }

    render() {
        return (
            <form className="city_form">
                <div className="row space_wrapper">
                    <div className="col-xs-8">
                        <input type="text" className="form-control city-input" placeholder="City name" onChange={this.onCityInputChange} value={this.state.city.city}></input>
                    </div>
                    <button className="btn btn-success col-xs-3 col-xs-offset-1" onClick={this.onAddClick}>Add</button>
                </div>
                <div className="row space_wrapper">
                    <button className="btn btn-warning col-xs-3 col-xs-offset-9">Update</button>
                </div>
                <div className="row space_wrapper">
                    <button className="btn btn-danger col-xs-3 col-xs-offset-9">Delete</button>
                </div>
            </form>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        cities: state.cities
    };
}

export default connect(mapStateToProps)(CityForm);