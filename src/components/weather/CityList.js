import React from "react";
import PropTypes from "prop-types";

class City extends React.Component {
    render() {
        return (
            <li className="city-list-element text-center">{this.props.city}</li>
        );
    }
}

City.propTypes = {
    city: PropTypes.string.isRequired
};

class CityList extends React.Component {
    render() {
        return (
            <div className="city-list-block">
                <ul className="city-list">
                    <City city="Minsk" />
                    <City city="Moscow" />
                    <City city="London" />
                    <City city="Minsk" />
                    <City city="Moscow" />
                    <City city="London" />
                    <City city="Minsk" />
                    <City city="Moscow" />
                    <City city="London" />
                    <City city="Minsk" />
                    <City city="Moscow" />
                    <City city="London" />
                    <City city="Minsk" />
                    <City city="Moscow" />
                    <City city="London" />
                    <City city="Minsk" />
                    <City city="Moscow" />
                    <City city="London" />
                    <City city="Minsk" />
                    <City city="Moscow" />
                    <City city="London" />
                </ul>
            </div>
        );
    }
}

export default CityList;