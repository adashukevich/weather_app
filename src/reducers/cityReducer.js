export default function cityReducer(state = [], action) {
    switch(action.type) {
        case "ADD_CITY": 
            return [...state,
                Object.assign({}, action.city)
            ];
            
        default: 
            return state;
    }
}