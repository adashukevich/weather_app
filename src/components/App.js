import React from "react";
import CityForm from "./weather/CityForm.js";
import CityList from "./weather/CityList.js";
import WeatherView from "./weather/WeatherView.js";

class App extends React.Component {
    render() {
        return (
            <div className="container-fluid fullscreen">
                <div className="col-xs-3 left-side-block">
                    <CityForm />
                    <CityList />
                </div>
                <WeatherView />
            </div>
        );
    }
}

export default App;